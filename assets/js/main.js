/*==================== SHOW MENU ====================*/
const showMenu = (toggleId, navId) =>{
    const toggle = document.getElementById(toggleId),
    nav = document.getElementById(navId)
    
    // Validate that variables exist
    if(toggle && nav){
        toggle.addEventListener('click', ()=>{
            nav.classList.toggle('show-menu')
        })
    }
}
showMenu('nav-barra','nav-menu')


/*==================== Comentarios ====================*/
var arrayAlerts =[];
const alerts= document.getElementById("alert");

var autor='';
var texto='';

function alertas(autor,comentario,fecha){
    alerts.innerHTML = arrayAlerts.map(alert=>
        `<div class="alert alert-primary" role="alert">
        <h4>Autor: ${alert.autor} </h4>
        <h4>Fecha: ${alert.fecha} </h4>
        <h4>Comentario: ${alert.texto} </h4>
      </div>`).join('');
}

function capturar(){
    autor= document.getElementById("autor").value;
    texto= document.getElementById("texto").value;
    if(autor==""||texto==""){
        alert("Llene todo los campos")
    }else{
        if(autor.length<3||texto.length>200){
            if(autor.length<3){
                alert("El autor debe tener almenos 3 caracteres")
            }
            if(texto.length>200){
                alert("El comentario no deve exeder los 200 catacteres")
            }
        }else{
            if(arrayAlerts.length==4){
                alert("hola")
                eliminarComentario();
            }
            arrayAlerts.push({autor:autor,texto:texto,fecha:calcularFecha()});
            alertas(autor,texto,calcularFecha());
            document.getElementById("autor").value="";
            document.getElementById("texto").value="";
        }
        
    }
    console.log(arrayAlerts)
}

function eliminarComentario(){
    arrayAlerts.splice(0,1);
}

function calcularFecha() {
    let date = new Date();
    var options = {
      year: "numeric",
      month: "long",
      day: "numeric",
      hour: "numeric",
      minute: "numeric",
    };
    var fecha = date.toLocaleDateString("es-ES", options);
    return fecha;
  }

  